// soal 1
const luas=(p , l) =>{
    return p * l 
}

let p = 13
let l = 4
let luas_persegi_panjang = luas(p , l)
console.log("Luas Persegi panjang = "+luas_persegi_panjang)

const keliling=(p2 , l2) => {
    return 2*p2 + 2*l2 
}

let p2 = 13
let l2 = 4
let keliling_persegi_panjang = keliling(p2 , l2)
console.log("Keliling Persegi panjang = "+keliling_persegi_panjang) // Jawaban soal 1

console.log('\n')

//soal 2
const newfunction = function literal(firstname,lastname){
    return{
    
    firstname: firstname,
    lastname: lastname,
    fullname : function(){
        console.log(firstname + " " + lastname)
        return
       }
     }
    }
    
    //drive code
    newfunction("Nugi", "Nugraha").fullname()//jawaban soal 2

    console.log('\n')


//soal 3
const newObject = {
    NamaAwal: "Nugi",
    NamaPanjang: "Nugraha",
    Alamat: "Cempaka Warna",
    Hobi: "Bermain Game",
}

const {NamaAwal, NamaPanjang, Alamat, Hobi} = newObject;

// Driver code
console.log(NamaAwal, NamaPanjang, Alamat, Hobi); // jawaban soal 3

console.log('\n')

//soal 4
const west = ["Will", "Chris", "Sam", "Holly,"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = (west+east)

//Driver Code
console.log(combined);// jawaban soal 4

console.log('\n')

//soal 5
const planet = "earth" 
const view = "glass" 
var after = 'Lorem ' + `${view}` + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + `${planet}` 

console.log(after)//jawaban soal 5