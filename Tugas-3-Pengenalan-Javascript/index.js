// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var ketiga =  pertama.substring(0, 4);
var keempat = pertama.substring(12, 18);
var kelima = kedua.substring(0, 7);
var keenam = kedua.substring(8, 18);
var ketujuh = keenam.toUpperCase();

console.log(ketiga, keempat, kelima, ketujuh) // jawaban soal 1

// soal 2

var kataPertama = 10;
var kataKedua = 2;
var kataKetiga = 4;
var kataKeempat = 6;
var hasiltambah = kataPertama + kataKedua;
var hasilmodulus = kataKeempat % kataKetiga;
var hasilperkalian = hasiltambah * hasilmodulus;

console.log("(10 + 2) x (6 mod 4) =", hasilperkalian) // jawaban soal 2

// soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima); // jawaban soal 3